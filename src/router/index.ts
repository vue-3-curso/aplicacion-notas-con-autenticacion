import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import LoginView from '@/views/LoginView.vue'
import ReadNotesView from '@/views/ReadNotesView.vue'
import RegisterView from '@/views/RegisterView.vue'
import CreateNoteView from '@/views/CreateNoteView.vue'
import { useAuth } from '@/store/auth'


const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'login',
    component: LoginView,
    meta:{
      requireAuth:false
    }
  },
  {
    path: '/',
    name: 'register',
    component: RegisterView,
    meta:{
      requireAuth:false
    }
  },
  {
    path: '/',
    name: 'read',
    component: ReadNotesView,
    meta:{
      requireAuth:true
    }
  },
  {
    path: '/',
    name: 'create',
    component: CreateNoteView,
    meta:{
      requireAuth:true
    }
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to,from,next)=>{
  const auth=useAuth()
  const isAuth=auth.token

  if(to.meta.requireAuth && isAuth==null){
    next('login')
  }else{
    next()
  }

})

export default router
